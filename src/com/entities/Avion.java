package com.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity @Table( name = "avions")
public class Avion implements Serializable {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long numero;
    private String compagnie;
    private String constructeur;
    private String modele;
    private Integer capacite;

    public Avion() {
    }

    public Long getNumero() {
        return numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public String getCompagnie() {
        return compagnie;
    }

    public void setCompagnie(String compagnie) {
        this.compagnie = compagnie;
    }

    public String getConstructeur() {
        return constructeur;
    }

    public void setConstructeur(String constructeur) {
        this.constructeur = constructeur;
    }

    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public Integer getCapacite() {
        return capacite;
    }

    public void setCapacite(Integer capacite) {
        this.capacite = capacite;
    }
}
