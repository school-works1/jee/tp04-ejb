package com.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity @Table(name = "passagers")
public class Passager implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long numero;
    private String nom;
    private String prenom;

    @ManyToMany
    @JoinTable(name = "reservations", joinColumns = @JoinColumn(name = "id_passager"),
            inverseJoinColumns = @JoinColumn(name = "id_vol"))
    private Collection<Vol> vols;

    public Passager() {
    }

    public Long getNumero() {
        return numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Collection<Vol> getVols() {
        return vols;
    }

    public void setVols(Collection<Vol> vols) {
        this.vols = vols;
    }
}
