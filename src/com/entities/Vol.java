package com.entities;

import javax.persistence.*;
import javax.persistence.criteria.Fetch;
import java.io.Serializable;
import java.util.Collection;

@Entity @Table(name = "vols")
public class Vol implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long numero;
    private int jourSem;
    private int jour;
    private int placeLibre;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_avion")
    private Avion avion;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_horaire")
    private Horaire horaire;

    @ManyToMany
    @JoinTable(name = "reservations", joinColumns = @JoinColumn(name = "id_vol"),
            inverseJoinColumns = @JoinColumn(name = "id_passager"))
    private Collection<Passager> passagers;

    public Vol() {
    }

    public Long getNumero() {
        return numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public int getJourSem() {
        return jourSem;
    }

    public void setJourSem(int jourSem) {
        this.jourSem = jourSem;
    }

    public int getJour() {
        return jour;
    }

    public void setJour(int jour) {
        this.jour = jour;
    }

    public int getPlaceLibre() {
        return placeLibre;
    }

    public void setPlaceLibre(int placeLibre) {
        this.placeLibre = placeLibre;
    }

    public Avion getAvion() {
        return avion;
    }

    public void setAvion(Avion avion) {
        this.avion = avion;
    }

    public Horaire getHoraire() {
        return horaire;
    }

    public void setHoraire(Horaire horaire) {
        this.horaire = horaire;
    }

    public Collection<Passager> getPassagers() {
        return passagers;
    }

    public void setPassagers(Collection<Passager> passagers) {
        this.passagers = passagers;
    }
}
