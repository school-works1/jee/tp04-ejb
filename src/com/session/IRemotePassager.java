package com.session;

import com.entities.Passager;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface IRemotePassager {
    Passager addPassager(Passager a);
    Passager deletePassager(Passager a);
    Passager updatePassager(Passager a);
    Passager getPassager(Long id);
    List<Passager> getAllPassager();
}
