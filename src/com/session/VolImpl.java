package com.session;

import com.entities.Vol;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Stateless(name = "Vol")
public class VolImpl implements IRemoteVol{

    private EntityManager em;

    @Override
    public Vol addVol(Vol a) {
        em.persist(a);
        return a;
    }

    @Override
    public Vol deleteVol(Vol a) {
        em.remove(a);
        return a;
    }

    @Override
    public Vol updateVol(Vol a) {
        em.persist(a);
        return a;
    }

    @Override
    public Vol getVol(Long id) {
        Vol found = em.find(Vol.class, id);
        if ( found == null ) throw new RuntimeException("Vol not found with id " +id);
        return found;
    }

    @Override
    public List<Vol> getAllVol() {
        return new ArrayList<>();
    }
}
