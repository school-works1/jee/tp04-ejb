package com.session;

import com.entities.Passager;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Stateless(name = "Passager")
public class PassagerImpl implements IRemotePassager{

    private EntityManager em;

    @Override
    public Passager addPassager(Passager a) {
        em.persist(a);
        return a;
    }

    @Override
    public Passager deletePassager(Passager a) {
        em.remove(a);
        return a;
    }

    @Override
    public Passager updatePassager(Passager a) {
        em.persist(a);
        return a;
    }

    @Override
    public Passager getPassager(Long id) {
        Passager found = em.find(Passager.class, id);
        if ( found == null ) throw new RuntimeException("Passager not found with id " +id);
        return found;
    }

    @Override
    public List<Passager> getAllPassager() {
        return new ArrayList<>();
    }
}
