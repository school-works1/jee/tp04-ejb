package com.session;

import com.entities.Horaire;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Stateless(name = "Horaire")
public class HoraireImpl implements IRemoteHoraire{

    private EntityManager em;

    @Override
    public Horaire addHoraire(Horaire a) {
        em.persist(a);
        return a;
    }

    @Override
    public Horaire deleteHoraire(Horaire a) {
        em.remove(a);
        return a;
    }

    @Override
    public Horaire updateHoraire(Horaire a) {
        em.persist(a);
        return a;
    }

    @Override
    public Horaire getHoraire(Long id) {
        Horaire found = em.find(Horaire.class, id);
        if ( found == null ) throw new RuntimeException("Horaire not found with id " +id);
        return found;
    }

    @Override
    public List<Horaire> getAllHoraire() {
        return new ArrayList<>();
    }
}
