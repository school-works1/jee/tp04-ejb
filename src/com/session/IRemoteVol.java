package com.session;

import com.entities.Vol;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface IRemoteVol {
    Vol addVol(Vol a);
    Vol deleteVol(Vol a);
    Vol updateVol(Vol a);
    Vol getVol(Long id);
    List<Vol> getAllVol();
}
