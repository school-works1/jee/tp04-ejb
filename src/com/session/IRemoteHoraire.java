package com.session;

import com.entities.Horaire;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface IRemoteHoraire {
    Horaire addHoraire(Horaire a);
    Horaire deleteHoraire(Horaire a);
    Horaire updateHoraire(Horaire a);
    Horaire getHoraire(Long id);
    List<Horaire> getAllHoraire();
}
