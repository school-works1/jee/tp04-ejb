package com.session;

import com.entities.Avion;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface IRemoteAvion {
    Avion addAvion(Avion a);
    Avion deleteAvion(Avion a);
    Avion updateAvion(Avion a);
    Avion getAvion(Long id);
    List<Avion> getAllAvion();
}
