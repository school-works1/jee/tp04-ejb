package com.session;

import com.entities.Avion;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Stateless(name = "Avion")
public class AvionImpl implements IRemoteAvion{

    private EntityManager em;

    @Override
    public Avion addAvion(Avion a) {
        em.persist(a);
        return a;
    }

    @Override
    public Avion deleteAvion(Avion a) {
        em.remove(a);
        return a;
    }

    @Override
    public Avion updateAvion(Avion a) {
        em.persist(a);
        return a;
    }

    @Override
    public Avion getAvion(Long id) {
        Avion found = em.find(Avion.class, id);
        if ( found == null ) throw new RuntimeException("Avion not found with id " +id);
        return found;
    }

    @Override
    public List<Avion> getAllAvion() {
        return new ArrayList<>();
    }
}
